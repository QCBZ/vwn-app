# vwn-app

#### 项目介绍
vwm的移动端平台，使用AngularJS 5.2.11+Ionic 3.9.2+Cordova+Typescript2.6.2搭建，一款接近原生的Html5移动App开发框架 创建精彩的应用 从这里开始。
Ionic 是目前最有潜力的一款 HTML5 手机应用开发框架。通过 SASS 构建应用程序，它 提供了很多 UI 组件来帮助开发者开发强大的应用。 它使用 JavaScript MVVM 框架和 AngularJS 来增强应用。提供数据的双向绑定，使用它成为 Web 和移动开发者的共同选择。

#### 开发前准备
开发基础知识
移动应用开发平台使用 Ionic 框架开发移动 App，因此需要学习 Ionic 技术栈相关基础知识。

1.HTML、CSS 和 JavaScript

2.TypeScript
http://www.typescriptlang.org/index.html

3.Angular
https://angular.io

4.Ionic
https://ionicframework.com

5.Cordova
https://cordova.apache.org

#### 软件架构
软件架构说明
VWN的移动端平台，使用AngularJS 5.2.11+Ionic 2.1.9+Cordova+Typescript2.6.2搭建

#### 安装教程
1.安装 Node 和 NPM
从Node.js官网上 https://nodejs.org/ 下载对应平台的安装包安装并验证是否安装成功。
node --version  
npm --version  
安装 nrm 用于管理 npm 源。
npm install -g nrm  
2.安装 Git
打开 https://git-scm.com/ 下载安装包进行安装并验证安装是否成功。Windows用户可安装 Git Bash 使用命令行工具。
git --version  
3.安装 Cordova CLI
npm install -g cordova  
4.安装 Ionic CLI
npm install -g ionic cordova  
5.安装平台工具
如果需要在模拟器或设备上开发调试，需要安装原生 App 平台工具。iOS安装 Xcode，Android 安装Android Studio。
6.开发工具，推荐使用 Visual Studio Code。
https://code.visualstudio.com/

#### 使用说明

1. cd vwn-app //调到项目目录下
2. npm install //安装nodejs依赖包
3. ionic serve //运行项目
4. http://localhost:8100 //访问项目

#### 界面预览

!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/1.png)
!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/2.png)
!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/3.png)
!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/4.png)
!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/5.png)
!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/6.png)
!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/7.png)
!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/8.png)
!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/9.png)
!["APP整体界面"](https://gitee.com/QCBZ/vwn-app/raw/master/doc/10.png)





#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://gitee.com/QCBZ/vwn-app)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

