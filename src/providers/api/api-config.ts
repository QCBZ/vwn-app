import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomainType } from '../common/enum';

/**
 * 统一配置接口地址
 *
 * @export
 * @class ApiConfig
 */

@Injectable()
export class ApiConfig {

  private static instance: ApiConfig;

  private constructor () {}

  static getInstance (): ApiConfig {
    if (!ApiConfig.instance) {
      ApiConfig.instance = new ApiConfig();
    }
    return this.instance;
  }

  /**
   * 获取域名
   */
  getDomainInfo(domainType: DomainType): any {
    let domain: string;
    switch (domainType) {
      case DomainType.PRODUCT: domain = "http://10.111.76.180:9999"; break;
      case DomainType.DEVELOP: domain = "http://10.111.243.39:9999"; break;
      case DomainType.PROXY: domain = "/proxyApi"; break;
      case DomainType.LOCAL: domain = ""; break;
      default: domain = ""; break;
    }
    return { domain: domain, domainType: domainType };
  }

  /**
   *获取api地址
   */
  getApiHost(): string{
      return this.getDomainInfo(DomainType.PROXY).domain + "";
  }

  readonly defaultHeaders = new HttpHeaders({'Content-Type': 'application/json', 'Accept': 'application/json'});
  readonly formHeaders = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'application/json'});
  readonly uploadHeasers = new HttpHeaders({'Content-Type': 'multipart/form-data'});
  //
  readonly defaultOptions: any = {headers: this.defaultHeaders};
  readonly formOptions: any = {headers: this.formHeaders};
  readonly uploadOptions: any = {headers: this.uploadHeasers};

  //接口地址配置
  //用户模块
  readonly loginUrl ="API/Autoops/Sys/login";  //登录接口
  readonly getAllOrgListByParentidUrl ="API/autoops/VOrgUserREST/getAllByParentid";  //获取用户单位列表接口
  //公用模块
  readonly getAllByTypeidAndStatusUrl ="API/autoops/SysBaseDimisionREST/getAllByTypeidAndStatus";  //获取下拉框列表数据接口

  //请假管理模块
  readonly insertUserLeaveUrl ="API/autoops/SysUserLevelREST/insert";  //保存用户请假信息接口
  readonly deleteUserLevelUrl ="API/autoops/SysUserLevelREST/deleteByLevel_id";  //保存用户请假信息接口
  readonly updateUserLevelUrl ="API/autoops/SysUserLevelREST/update";  //更新用户请假信息接口
  readonly getAllUserLevelByApplyUserUrl ="API/autoops/SysUserLevelREST/getAllByApply_user";  //通过用户ID获取用户请假信息接口
  readonly getAllUserLevelUrl ="API/Autoops/SysUserLevel/GetSysUserLevelList";  //获取用户请假信息接口
  readonly getNewUserLevelUrl ="API/autoops/SysUserLevelREST/getNew";  //获取请假编号接口

}
