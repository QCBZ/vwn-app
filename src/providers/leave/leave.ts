import { Injectable } from '@angular/core';

import { Leave } from '../../models/leave';
import { Api } from '..';
import {ApiConfig} from "../api/api-config";

/**
 * @description 请假页面方法处理
 * @author 李元坝
 * @date 20180611
 */

@Injectable()
export class Leaves {

  leaves: Leave[] = [];

  constructor(public api: Api) { }

  defaultLeave: any = {};

  getAllByApplyUser(params?: any) {
    return this.api.post(ApiConfig.getInstance().getAllUserLevelUrl, params);
  }
  getNew(params?: any) {
    return this.api.post(ApiConfig.getInstance().getNewUserLevelUrl, params);
  }
  insert(params?: any) {
    return this.api.post(ApiConfig.getInstance().insertUserLeaveUrl, params);
  }

  delete(params?: any) {
    return this.api.post(ApiConfig.getInstance().deleteUserLevelUrl, params);
  }

}
