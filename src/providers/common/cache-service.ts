import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

/**
 * 缓存工具类
 */

export enum CacheKeys {
  LAUNCHED, //标记是否首次运行
  APPVERSION, //App 版本
  TOKEN,//token
  User,//用户信息
}

@Injectable()
export class CacheService {

  constructor(public storage: Storage) {
  }

  get(key: CacheKeys) : Promise<any>{
    return this.storage.get(CacheKeys[key])
  }

  set(key: CacheKeys, value: any): Promise<any>{
    return this.storage.set(CacheKeys[key], value);
  }

  clear(){
    return this.storage.clear();
  }

  remove(key: CacheKeys): Promise<any>{
    return this.storage.remove(CacheKeys[key]);
  }

}
