import { Injectable } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Observable } from 'rxjs';
import { AppVersion } from '@ionic-native/app-version';
import { TranslateService } from '@ngx-translate/core';
import { ENABLE_VPN } from './constants';
import { CacheService, CacheKeys } from './cache-service';
import { RootPage } from '../../pages/index';

/**
 * App 管理类
 */
@Injectable()
export class AppService {

  constructor(private screenOrientation: ScreenOrientation,
              private translate: TranslateService,
              private cacheService: CacheService,
              private appVersion: AppVersion) {
    //
  }

  /**
   * 国际化语言设置
   */
  initTranslate() {
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        this.translate.use('zh-cmn-Hans');
      } else {
        this.translate.use('en');
      }
    } else {
      this.translate.use('en'); 
    }
  }

  /**
   * 设置首页
   */
  initRootPage(): Observable<string> {
    //移动云网集成页
    if (ENABLE_VPN) {
      return Observable.create(observer => {
        observer.next('VpnPage');
      });
    }

    //首次运行时显示引导页
    return Observable.create(observer => {
      this.cacheService.get(CacheKeys.LAUNCHED).then(launched => {
        if(!launched) {
          observer.next('TutorialPage');
        }else {
          observer.next(RootPage);
        }
      }).catch(err => {
        observer.error(RootPage);
      });
    });
  }

  /**
   * 锁定竖屏
   */
  lockPortrait() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  /**
   * 锁定横屏
   */
  lockLandscape() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
  }

  /**
   * 解除屏幕锁定
   */
  unlockOrientation() {
    this.screenOrientation.unlock();
  }

  /**
   * 监听屏幕旋转
   */
  onScreenChange(): Observable<void> {
    return this.screenOrientation.onChange();
  }

  /**
   * 获取 App 版本号
   * @description  对应/config.xml中version的值
   */
  getVersionNumber(): Observable<string> {
    return Observable.create(observer => {
      this.appVersion.getVersionNumber().then((value: string) => {
        observer.next(value);
      }).catch(err => {
        observer.error('获取版本号失败');
      });
    });
  }

  /**
   * 获取 App name
   * @description  对应/config.xml中name的值
   */
  getAppName(): Observable<string> {
    return Observable.create(observer => {
      this.appVersion.getAppName().then((value: string) => {
        observer.next(value);
      }).catch(err => {
        observer.error('获取 App name 失败');
      });
    });
  }

  /**
   * 获取 App 包名/id
   * @description  对应/config.xml中id的值
   */
  getPackageName(): Observable<string> {
    return Observable.create(observer => {
      this.appVersion.getPackageName().then((value: string) => {
        observer.next(value);
      }).catch(err => {
        observer.error('获取包名失败');
      });
    });
  }

}
