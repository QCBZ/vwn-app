import { Theme } from './enum';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

/**
 * 主题切换
 */
@Injectable()
export class ThemeService {

  private theme: BehaviorSubject<String>;

  constructor() {
    // theme 是 BehaviorSubject实例
    this.theme = new BehaviorSubject(Theme.ThemeDefault);
  }

  setActiveTheme(val) {
    // 改变值
    this.theme.next(val);
  }

  getActiveTheme() {
    // 观察
    return this.theme.asObservable();
  }

}
