import { Injectable } from '@angular/core';
import { ThemeableBrowser, ThemeableBrowserOptions } from '@ionic-native/themeable-browser';

@Injectable()
export class NativeBrowser {

  constructor(private themeableBrowser: ThemeableBrowser) {
    //
  }

  show(url: string, opts?: ThemeableBrowserOptions) {
    if (!opts) {
      opts = {
        statusbar: {
          color: '#ffffffff'
        },
        toolbar: {
          height: 44,
          color: '#ffffffff'
        },
        title: {
          color: '#003264ff',
          showPageTitle: true
        },
        closeButton: {
          wwwImage: 'assets/img/browser-close.png',
          wwwImagePressed: 'assets/img/browser-close.png',
          wwwImageDensity: 2,
          align: 'left',
          event: 'closePressed'
        }
      };
    }

    this.themeableBrowser.create(url, '_blank', opts);
  }

}
