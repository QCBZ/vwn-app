import { ToastController, AlertController, AlertOptions, LoadingController, LoadingOptions, Loading } from 'ionic-angular';
import { Injectable } from '@angular/core';

/*
  通用提示框
*/
@Injectable()
export class Prompt {

  constructor(private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController) {
    // console.log('Hello DialogProvider Provider');
  }

  /*********************Toast*************************/

  showToast(message: string, position: string = 'top', duration: number = 2000, cssClass: string=''): Promise<any> {
    let toast = this.toastCtrl.create({
      message: message,
      cssClass: cssClass,
      duration: duration,
      position: position
    });
    return toast.present(toast);
  }

  /*********************Alert*************************/

  showCommonAlert(opts?: AlertOptions) {
    if(!opts){
      opts = {
        title: '温馨提示',
        buttons: ['确定']
      };
    }
    let alert = this.alertCtrl.create(opts);
    alert.present();
  }

  presentAlert(message: string, confirmCallback?: Function) {
    let buttons: any[] = [{
      text: '确定',
      handler: confirmCallback
    }];

    let opts: AlertOptions = {
      title: '温馨提示',
      message: message,
      enableBackdropDismiss: false,
      buttons: buttons
    };

    let alert = this.alertCtrl.create(opts);
    alert.present();
  }

  presentConfirm(message: string, confirmText?:string, cancelText?:string, confirmCallback?: Function, cancelCallback?: Function) {
    let buttons: any[] = [{
          text: cancelText || '取消',
          role: 'cancel',
          handler: cancelCallback
        },
        {
          text: confirmText || '确定',
          handler: confirmCallback
        }
    ];

    let opts: AlertOptions = {
      title: '温馨提示',
      message: message,
      enableBackdropDismiss: false,
      buttons: buttons
    };

    let alert = this.alertCtrl.create(opts);
    alert.present();
  }


  /*********************Loading*************************/

  createLoading(opts?: LoadingOptions, callback?: Function): Loading {
    if(!opts){
       opts = {
         content: "请稍后...",
         showBackdrop: false,
         duration: 3000
       };
    }
    let loader = this.loadingCtrl.create(opts);
    if(callback != null){
      loader.onDidDismiss((data, role) =>callback(data, role));
    }
    return loader;
  }

  createCommonLoading(content: string, callback?: Function): Loading {

    let opts = {
      content: content
    };

    let loader = this.loadingCtrl.create(opts);
    if(callback != null){
      loader.onDidDismiss((data, role) =>callback(data, role));
    }
    return loader;
  }

}
