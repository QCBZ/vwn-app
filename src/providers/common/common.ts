import { Injectable } from '@angular/core';

import { Api } from '..';
import {ApiConfig} from "../api/api-config";

/**
 * @description 公用方法处理
 * @author 李元坝
 * @date 20180613
 */

@Injectable()
export class Commons {

  constructor(public api: Api) { }

  getAllByTypeidAndStatus(params?: any) {
    return this.api.post(ApiConfig.getInstance().getAllByTypeidAndStatusUrl, params);
  }



}
