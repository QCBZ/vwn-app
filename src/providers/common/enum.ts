
/**
 * 域名
 */
export enum DomainType {
    DEVELOP, //开发环境
    PRODUCT, //生产环境
    PROXY, //代理
    LOCAL //本地
}

/**
 * 主题
 */
export enum Theme {
    ThemeDefault = 'ionic.theme.default',
    ThemeLight = 'light-theme',
    ThemeDark = 'theme-dark',
}
