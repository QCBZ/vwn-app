import { AppService } from './../providers/common/app-service';
import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';

import { BackBtnService } from '../providers/common/back-btn-service';
import { ThemeService } from '../providers/common/theme-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: string;
  selectedTheme: String;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: '首页', component: 'TabsPage' },
    { title: '搜索', component: 'SearchPage' }
  ]

  constructor(private translate: TranslateService,
              private platform: Platform,
              private config: Config,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private backBtnService: BackBtnService,
              private themeService: ThemeService,
              private appService: AppService) {

    this.initializeApp();
  }

  initializeApp() {

    this.platform.ready().then((readySource) => {
      this.statusBar.styleDefault();
      //this.splashScreen.hide();

      //设置首页
      this.appService.initRootPage().subscribe(page => {
        this.rootPage = page;
        setTimeout(() => {
          this.splashScreen.hide();
        }, 1000);
      });

      if(readySource === 'cordova') {
        //处理安卓硬件返回
        this.backBtnService.registerBackButtonAction();
        //竖屏锁定
        this.appService.lockPortrait();
      }
    });

    //国际化设置
    this.appService.initTranslate();

    //主题设置
    this.themeService.getActiveTheme().subscribe(val => this.selectedTheme = val);

    //返回按钮文字
    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
