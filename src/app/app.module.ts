import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { NativeBrowser } from '../providers/common/native-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { IonicStorageModule } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MyApp } from './app.component';
import { ThemeableBrowser } from '@ionic-native/themeable-browser';
import { BackBtnService } from '../providers/common/back-btn-service';
import { BrowserPopover } from '../pages/common/browser/browser-popover';
import { ThemeService } from '../providers/common/theme-service';
import { AppService } from '../providers/common/app-service';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppVersion } from '@ionic-native/app-version';
import { CacheService,Leaves ,User, Api, Prompt,Commons} from '../providers';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    BrowserPopover
  ],
  imports: [
    BrowserModule,  HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BrowserPopover
  ],
  providers: [
    Api,
    Leaves,
    Commons,
    User,
    Camera,
    SplashScreen,
    StatusBar,
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Prompt,
    NativeBrowser,
    ThemeableBrowser,
    BackBtnService,
    ThemeService,
    AppService,
    AppVersion,
    ScreenOrientation,
    CacheService
  ]
})
export class AppModule {}
