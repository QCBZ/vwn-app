import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * @description 主页面数据处理
 * @author 李元坝
 * @date 20180611
 */
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  cardItems: any[];

  constructor(public navCtrl: NavController) {
    this.cardItems = [
      {
        user: {
          avatar: 'assets/img/avatar.png',
          name: 'Ionic'
        },
        date: 'May 5, 2018',
        image: 'assets/img/advance-card-top1.png',
        content: 'Ionic is the beautiful, free and open source mobile SDK for developing native and progressive web apps with ease.',
      }
    ];

  }
}
