import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, MenuController} from 'ionic-angular';
import {Leave} from "../../../models/leave";
import {CacheKeys, Leaves, CacheService, Prompt} from '../../../providers';


/**
 * @description 请假列表页面数据处理
 * @author 李元坝
 * @date 20180611
 */
@IonicPage()
@Component({
  selector: 'page-leave-list',
  templateUrl: 'leave-list.html'
})
export class LeaveListPage {
  currentLeaves: any[];
  user: any;

  constructor(public navCtrl: NavController, public menu: MenuController, public leaves: Leaves, public cacheService: CacheService, private prompt: Prompt, public modalCtrl: ModalController) {
    // 获取用户信息
    this.cacheService.get(CacheKeys.User).then((r: any) => {
      if (r == null) {
        this.prompt.showToast('获取用户信息失败，请重新登录', 'top', 1000);
        return false
      }
      this.user = r.user
      //获取请假列表
      let param = {apply_user: this.user.userid, pageNum: 1, pageSize: 10}
      this.leaves.getAllByApplyUser(param).subscribe((r: any) => {
        this.currentLeaves = r.sysUserLevelList.list;
      });
    }).catch()


  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    this.menu.enable(true);
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addLeave() {
    let addModal = this.modalCtrl.create('LeaveCreatePage');
    addModal.onDidDismiss(leave => {
      if (leave) {
        //保存后重新获取请假列表，刷新数据
        let param = {apply_user: leave.apply_user, pageNum: 1, pageSize: 10};
        this.leaves.getAllByApplyUser(param).subscribe((r: any) => {
          this.currentLeaves = r.sysUserLevelList.list;
        })
      }
    });
    addModal.present();

  }

  /**
   * Delete an leave from the list of items.
   */
  deleteLeave(leave) {
    this.leaves.delete(leave);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openLeave(leave: Leave) {
    this.navCtrl.push('LeaveDetailPage', {
      leave: leave
    });
  }
}
