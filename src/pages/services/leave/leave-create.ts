import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IonicPage, Loading, NavController, ViewController} from 'ionic-angular';
import {Leaves, Commons, CacheKeys, CacheService, Prompt} from '../../../providers';

/**
 * @description 请假添加页面数据处理
 * @author 李元坝
 * @date 20180611
 */
@IonicPage()
@Component({
  selector: 'page-leave-create',
  templateUrl: 'leave-create.html'
})
export class LeaveCreatePage {

  isReadyToSave: boolean;
  leave: any;
  levelTypeDataArr: any[];
  time_min: any;
  form: FormGroup;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, formBuilder: FormBuilder, private leaves: Leaves, public cacheService: CacheService, private prompt: Prompt, private commons: Commons) {
    this.form = formBuilder.group({
      level_id: ['', Validators.required],
      level_type: ['', Validators.required],
      apply_memo: ['', Validators.required],
      start_time: ['', Validators.required],
      end_time: ['', Validators.required],
      orgid: [''],
      apply_user: [''],
      apply_time: [''],
      status: [''],
      confirm_user: [''],
      confirm_time: [''],
      confirm_memo: [''],
      sysUserLevelDetail: ['']
    });
    //初始化时间最小值
    this.time_min = new Date().toISOString();

    //初始化审批状态
    this.form.patchValue({status: 1});
    this.form.patchValue({apply_time: new Date().toISOString()});

    //获取请假编号
    let param = {obj: {}};
    this.leaves.getNew(param).subscribe((r: any) => {
      this.form.patchValue({level_id: r.obj.level_id});
    });

    //获取请假类型下拉框数据
    param = {obj: {typeid: 1071, dim_status: 1}};
    this.commons.getAllByTypeidAndStatus(param).subscribe((r: any) => {
      this.levelTypeDataArr = r.list;
    });

    // 获取用户信息
    this.cacheService.get(CacheKeys.User).then((r: any) => {
      if (r == null) {
        this.prompt.showToast('获取用户信息失败，请重新登录', 'bottom', 1000);
        return false
      }
      let user = r.user;
      this.form.patchValue({apply_user: user.userid});
      this.form.patchValue({orgid: user.orgid});
    }).catch();

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });

  }


  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   * 请假信息保存方法
   *
   */
  done() {
    if (!this.form.valid) {
      return;
    }

    let obj = this.form.value;//表单对象
    let sysUserLevelDetail = {
      id: null,
      level_id: obj.level_id,
      level_type: obj.level_type,
      start_time: new Date(obj.start_time),
      end_time: new Date(obj.end_time),
      sn: null
    };
    obj.sysUserLevelDetail = [];
    obj.sysUserLevelDetail.push(sysUserLevelDetail);

    let loading: Loading = this.prompt.createCommonLoading('正在保存');
    loading.present();
    let param = {obj: obj};//数据库表单对象
    this.leaves.insert(param).subscribe((r: any) => {
      loading.dismiss();
      if (r.obj) {
        this.viewCtrl.dismiss(r.obj);
      }
    }, (error: any) => {
      console.error(error)
      loading.dismiss();
      this.prompt.showToast(error, 'bottom', 1000);
    });


  }

}
