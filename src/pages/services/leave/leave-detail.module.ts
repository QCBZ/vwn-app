import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { LeaveDetailPage } from './leave-detail';

@NgModule({
  declarations: [
    LeaveDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveDetailPage),
    TranslateModule.forChild()
  ],
  exports: [
    LeaveDetailPage
  ]
})
export class LeaveDetailPageModule { }
