import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Leaves } from '../../../providers';



/**
 * @description 请假查看页面数据处理
 * @author 李元坝
 * @date 20180611
 */
@IonicPage()
@Component({
  selector: 'page-leave-detail',
  templateUrl: 'leave-detail.html'
})
export class LeaveDetailPage {
  leave: any;

  constructor(public navCtrl: NavController, navParams: NavParams, leaves: Leaves) {
    this.leave = navParams.get('leave') || leaves.defaultLeave;
  }

}
