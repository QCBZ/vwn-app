import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { LeaveListPage } from './leave-list';

@NgModule({
  declarations: [
    LeaveListPage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveListPage),
    TranslateModule.forChild()
  ],
  exports: [
    LeaveListPage
  ]
})
export class LeaveListPageModule { }
