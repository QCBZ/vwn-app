import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { LeaveCreatePage } from './leave-create';

@NgModule({
  declarations: [
    LeaveCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveCreatePage),
    TranslateModule.forChild()
  ],
  exports: [
    LeaveCreatePage
  ]
})
export class LeaveCreatePageModule { }
