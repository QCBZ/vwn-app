import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ServicesPage } from './services';

/**
 * @description 服务列表页面
 * @author 李元坝
 * @date 20180611
 */
@NgModule({
  declarations: [
    ServicesPage,
  ],
  imports: [
    IonicPageModule.forChild(ServicesPage),
    TranslateModule.forChild()
  ],
  exports: [
    ServicesPage
  ]
})
export class ServicesPageModule { }
