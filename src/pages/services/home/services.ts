import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {Menu} from "../../../models/menu";


/**
 * @description 服务列表页面数据处理
 * @author 李元坝
 * @date 20180611
 */
@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html'
})
export class ServicesPage {
  currentMenus: any[];
  menuList = [{
    "id": "001",
    "name": "请假管理",
    "profilePic": "assets/img/ica-slidebox-img-2.png",
    "about": "申请请假.",
  },{
    "id": "002",
    "name": "请假审批",
    "profilePic": "assets/img/ica-slidebox-img-3.png",
    "about": "请假审批.",
  }]

  constructor(public navCtrl: NavController) {
    this.currentMenus = this.menuList
  }

  /**
   * Navigate to the detail page for this item.
   */
  openMenu(menu: Menu) {
    if (menu.id == "001") {
      this.navCtrl.push('LeaveListPage', {
        menu: menu
      });
    } else if (menu.id == "002") {

    } else {

    }

  }
}
