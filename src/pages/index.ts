/**
 * @description 主列表页面
 * @author 李元坝
 * @date 20180611
 */
//设置 App 首页,引导页完成或者集成移动云网 vpn 认证成功后进入该页面
// export const RootPage = 'TabsPage';
export const RootPage = 'LoginPage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'HomePage';
export const Tab2Root = 'ServicesPage';
export const Tab3Root = 'MinePage';
