import { CacheService, CacheKeys } from './../../../providers/common/cache-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, Platform } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';
import { RootPage } from '../../../pages';

export interface Slide {
  title: string;
  description: string;
  image: string;
}

/**
 * @description 首次加载引导页面数据处理
 * @author 李元坝
 * @date 20180611
 */

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {
  slides: Slide[];
  showSkip = true;
  dir: string = 'ltr';

  constructor(public navCtrl: NavController, translate: TranslateService, public platform: Platform, public cacheService: CacheService) {
    this.dir = platform.dir();
    translate.get(["TUTORIAL_SLIDE1_TITLE",
      "TUTORIAL_SLIDE1_DESCRIPTION",
      "TUTORIAL_SLIDE2_TITLE",
      "TUTORIAL_SLIDE2_DESCRIPTION",
      "TUTORIAL_SLIDE3_TITLE",
      "TUTORIAL_SLIDE3_DESCRIPTION",
    ]).subscribe(
      (values) => {
        // console.log('Loaded values', values);
        this.slides = [
          {
            title: values.TUTORIAL_SLIDE1_TITLE,
            description: values.TUTORIAL_SLIDE1_DESCRIPTION,
            image: 'assets/img/ica-slidebox-img-1.png',
          },
          {
            title: values.TUTORIAL_SLIDE2_TITLE,
            description: values.TUTORIAL_SLIDE2_DESCRIPTION,
            image: 'assets/img/ica-slidebox-img-2.png',
          },
          {
            title: values.TUTORIAL_SLIDE3_TITLE,
            description: values.TUTORIAL_SLIDE3_DESCRIPTION,
            image: 'assets/img/ica-slidebox-img-3.png',
          }
        ];
      });
  }

  startApp() {
    this.navCtrl.setRoot(RootPage, {}, {
      animate: true,
      direction: 'forward'
    });

    this.cacheService.set(CacheKeys.LAUNCHED, true);
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

}
