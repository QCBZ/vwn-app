import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {IonicPage,  NavController } from 'ionic-angular';
import { ThemeService } from '../../providers/common/theme-service';
import { Theme } from '../../providers/common/enum';
import { RootPage} from "../index";
import {CacheKeys, CacheService, User} from "../../providers";

/**
 * @description 我的页面数据处理
 * @author 李元坝
 * @date 20180611
 */
@IonicPage()
@Component({
  selector: 'page-mine',
  templateUrl: 'mine.html'
})
export class MinePage {

  // Our translated text strings
  // private loginErrorString: string;
  pageTitleKey: string = 'MINE_TITLE';
  pageTitle: string;
  isToggled: boolean = false;


  constructor(public translate: TranslateService,public navCtrl: NavController,  public user: User,   private cacheService: CacheService,
    private themeService: ThemeService) {

  }

  ionViewDidLoad(){
    this.translate.get(this.pageTitleKey).subscribe((res) => {
      this.pageTitle = res;
    })
  }

  notify() {
    if(this.isToggled) {
      this.themeService.setActiveTheme(Theme.ThemeLight);
      // this.themeService.setActiveTheme(Theme.ThemeDark);
    }else {
      this.themeService.setActiveTheme(Theme.ThemeDefault);
    }
  }
  /**
   * 用户登出方法
   * @author 李元坝
   * @date 2018-06-06
   *
   */
  logout(){
    this.user.logout();
    this.cacheService.remove(CacheKeys.User).catch();
    this.navCtrl.push(RootPage).catch();
  }
}
