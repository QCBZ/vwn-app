import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { MinePage } from './mine';

@NgModule({
  declarations: [
    MinePage,
  ],
  imports: [
    IonicPageModule.forChild(MinePage),
    TranslateModule.forChild()
  ],
  exports: [
    MinePage
  ]
})
export class MinePageModule { }
