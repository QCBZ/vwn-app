/**
 * @description 菜单实体类
 * @author 李元坝
 * @date 20180612
 */
export class Menu {

  constructor(fields: any) {
    // Quick and dirty extend/assign fields to this model
    for (const f in fields) {
      // @ts-ignore
      this[f] = fields[f];
    }
  }

}

export interface Menu {
  id : null,
  name : null,
  about : null,
  profilePic : null,
}
