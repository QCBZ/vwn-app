/**
 * @description 请假信息实体类
 * @author 李元坝
 * @date 20180611
 */
export class Leave {

  constructor(fields: any) {
    // Quick and dirty extend/assign fields to this model
    for (const f in fields) {
      // @ts-ignore
      this[f] = fields[f];
    }
  }

}

export interface Leave {
  level_id : null,
  orgid : null,
  apply_user : null,
  apply_time : null,
  apply_memo : null,
  status : null,
  confirm_user : null,
  confirm_time : null,
  confirm_memo : null,
  sysUserLevelDetail : null

}
